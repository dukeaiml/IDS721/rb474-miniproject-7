# rb474-miniproject-7

```
docker pull qdrant/qdrant

docker run -p 6333:6333 -p 6334:6334 \
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
    qdrant/qdrant
```

```
cargo add qdrant-client anyhow tonic tokio serde-json --features tokio/rt-multi-thread
```

Usage example from [the repo of Qdrant rust-client](https://github.com/qdrant/rust-client) suggests a solution on how to "ingest data into vector database":
```rust
for i in 0..10 {
    let payload: Payload = json!(
        {
            "foo": "Bar",
            "bar": 0 + i,
            "baz": {
                "qux": "quux"
            }
        }
    )
        .try_into()
        .unwrap();
    let points = vec![PointStruct::new(0, vec![12.; 10], payload)];
    client
        .upsert_points_blocking(collection_name, None, points, None)
        .await?;
}
```

and "perform queries and aggregations":
```rust
let search_result = client
    .search_points(&SearchPoints {
        collection_name: collection_name.into(),
        vector: vec![11.; 10],
        filter: Some(Filter::all([Condition::matches("bar", 12)])),
        limit: 10,
        with_payload: Some(true.into()),
        ..Default::default()
    })
    .await?;
```

Visualisation:

![](images/vis.png)